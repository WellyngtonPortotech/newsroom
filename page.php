<?php
/*
 * Template name: Página padrão
 */
?>
<?php get_header(); setup_postdata($post); ?>
<div class="site-body site-page">
    <div class="content-block">
        <?php the_content(); ?>
        <?php wp_link_pages(); ?>
    </div>
</div>
<?php get_footer(); ?>

