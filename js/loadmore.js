(function ($) {

    const loadPosts = function (ev) {
        const trigger = $(ev.currentTarget);
        const params = trigger.data();

        if(!trigger.hasClass('active')) {
            trigger.addClass('active');
            $.ajax({
                url: params.url,
                type: 'POST',
                data: {
                    action: 'loadmore',
                    idPage: params.idPage,
                    idSection: params.idSection,
                    postsPerPage: params.postsPerPage,
                    offset: params.offset || 0,
                    extra: params.extra
                },
                dataType: 'json'
            }).then(data => {
                if(data && data.html) {
                    trigger.parents('.category-list').find('.category-list-body').append(data.html);
                    params.offset = data.offset || -1;
                    if(params.offset == -1) {
                        trigger.addClass('no-display');
                    }
                    trigger.data(params);
                } else {
                    trigger.addClass('no-display');
                }
                trigger.removeClass('active');
            }, () => {
                trigger.removeClass('active');
            });
        }
    };

    const setup = function () {
        $('.list-load-trigger').on('click', loadPosts);
    };

    $(setup);

})(jQuery);
