(function ($, viewport) {

    let animationFrame;

    const handleScroll = function () {
        $('.nv-page-header').toggleClass('fixed', viewport.pageYOffset > 46);
    };

    const triggerScrollCheck = function () {
        viewport.cancelAnimationFrame(animationFrame);
        animationFrame = viewport.requestAnimationFrame(handleScroll);
    };

    const toggleMenu = function () {
        $('.nv-main-nav').toggleClass('active');
    };

    const closeMenu = function (evt) {
        evt.stopPropagation();
        $('.nv-main-nav').removeClass('active');
    };

    const closeSubmenu = function () {
        $('.nv-main-nav .menu-item-has-children.active').removeClass('active');
        $(viewport.document).off('click', closeSubmenu);
    };

    const openSubmenu = function (dad) {
        const li = $(dad);
        const mustOpen = !li.hasClass('active');
        closeSubmenu();
        if(mustOpen) {
            li.addClass('active');
            $(viewport.document).on('click', closeSubmenu);
        }
    };

    const toggleSubmenu = function (evt) {
        evt.preventDefault();
        evt.stopPropagation();
        const li = evt.currentTarget.parentNode;
        openSubmenu(li);
    };

    const init = function () {
        $('.nv-main-nav .nv-ic-menu').on('click', toggleMenu);
        $('.nv-main-nav .nv-ic-close').on('click', closeMenu);
        $('.nv-main-nav .menu-item-has-children > a').on('click', toggleSubmenu);
        $(viewport).on('scroll', triggerScrollCheck);
    };

    $(init);

})(jQuery, window);