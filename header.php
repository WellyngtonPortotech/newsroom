<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>"/>
    <link rel="alternate" type="application/rss+xml" title="RSS 2.0" href="<?php bloginfo('rss2_url'); ?>" />
    <link rel="alternate" type="application/atom+xml" title="Atom" href="<?php bloginfo('atom_url'); ?>" />
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri(); ?>/style.css"/>
    <?php
    wp_head();
    get_template_part('inc/blocks/meta-single-schema');
    ?>
</head>
<body itemscope itemtype="http://schema.org/WebPage" <?php body_class(); ?>>
<?php do_action('after_body_open_tag'); ?>

<div class="nv-page">
    <header class="nv-page-header">
        <div class="nv-content">
            <p class="nv-site-logo" itemprop="publisher" itemscope itemtype="http://schema.org/Organization">
                <a href="<?php echo home_url(); ?>">
                    <strong itemprop="name"><?php bloginfo('name'); ?></strong>
                </a>
            </p>
            <div class="nv-main-nav">
                <div class="nv-ic-menu"></div>
                <div class="nv-main-nav-content">
                    <div class="nv-main-nav-content-blocks">
                        <div class="nv-ic-close"></div>
                        <div class="nv-main-nav-blocks">
                            <?php
                            if(has_nav_menu('main-menu')) {
                                wp_nav_menu(array(
                                    'menu' => 'main-menu',
                                    'menu_class' => '',
                                    'container' => 'nav',
                                    'container_class' => 'nv-main-nav-links',
                                    'depth' => 2,
                                    'theme_location' => 'main-menu'
                                ));
                            }
                            if(has_nav_menu('header-ctas')) {
                                wp_nav_menu(array(
                                    'menu' => 'header-ctas',
                                    'menu_class' => '',
                                    'container' => 'div',
                                    'container_class' => 'nv-page-header-ctas',
                                    'depth' => 2,
                                    'theme_location' => 'header-ctas'
                                ));
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <section class="nv-search">
                <h6 class="no-display">Search</h6>
                <div class="nv-ic-search"></div>
                <form method="get" action="<?php echo esc_url(home_url( '/' )); ?>">
                    <div>
                        <div class="nv-ic-close"></div>
                        <?php
                        $searchQuery = isset($_GET['s']) && !empty($_GET['s']) ? $_GET['s'] : '';
                        ?>
                        <input class="nv-input nv-input-search" type="search" name="s" value="<?php echo $searchQuery; ?>" placeholder="O que você procura?"/>
                        <button class="nv-bt nv-bt-alpha" type="submit">Buscar</button>
                    </div>
                </form>
            </section>
        </div>
    </header>
