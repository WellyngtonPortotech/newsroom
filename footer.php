<footer class="nv-page-footer">
    <div class="nv-content">
        <div class="nv-page-footer-group">
            <p class="nv-site-logo">
                <a href="<?php echo home_url(); ?>">
                    <strong itemprop="name"><?php bloginfo('name'); ?></strong>
                </a>
            </p>
        </div>
        <div class="nv-page-footer-group"></div>
        <div class="nv-page-footer-group">
            <?php
            if(has_nav_menu('footer-menu')) {
                wp_nav_menu(array(
                    'menu' => 'footer-menu',
                    'menu_class' => '',
                    'container' => 'div',
                    'container_class' => 'nv-footer-nav',
                    'depth' => 2,
                    'theme_location' => 'footer-menu'
                ));
            }
            if(has_nav_menu('social-menu')) {
                wp_nav_menu(array(
                    'menu' => 'social-menu',
                    'menu_class' => '',
                    'container' => 'div',
                    'container_class' => 'nv-social-nav',
                    'depth' => 2,
                    'theme_location' => 'social-menu'
                ));
            }
            /*
            if(has_nav_menu('group-sites-menu')) {
                wp_nav_menu(array(
                    'menu' => 'group-sites-menu',
                    'menu_class' => '',
                    'container' => 'div',
                    'container_class' => 'nv-group-sites-menu',
                    'depth' => 2,
                    'theme_location' => 'group-sites-menu',
                    'link_before' => '<span>',
                    'link_after' => '</span>'
                ));
            }
            */
            ?>
            <!--
            BEGIN Static group logos
            Because client doesn't want links right now, this block replaces the group-sites-menu.
            -->
            <div class="nv-group-sites-menu">
                <ul>
                    <li><span></span></li>
                    <li><span></span></li>
                    <li><span></span></li>
                    <li><span></span></li>
                </ul>
            </div>
            <!--
            END Static group logos
            -->
        </div>
    </div>
</footer>

<div class="nv-ad-box nv-ad-mobile nv-ad-mobile-catfish">
    <div class="nv-ad nv-ad-smartphone-static-banner" id="div-gpt-ad-1605551547127-0"></div>
</div>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
