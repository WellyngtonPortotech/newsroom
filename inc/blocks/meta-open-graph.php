<!-- Facebook Open Graph -->
<meta property="og:title" content="<?php if(!function_exists('bp_is_active') ) { if (is_single() || is_page()) { echo esc_attr(get_the_title()); } else { bloginfo('name'); } }else { if(is_single() || is_page() && !is_buddypress()) { echo esc_attr(get_the_title()); } elseif(is_buddypress()){ wp_title(); } else { bloginfo('name'); }} ?>"/>
<meta property="og:description" content="<?php if (is_single()) { echo substr(strip_tags($post->post_content), 0, 200); echo '...';} else { bloginfo('description'); } ?>"/>
<meta property="og:url" content="<?php if(is_home() || is_front_page()) { echo esc_url(home_url('/')); } else { the_permalink(); } ?>"/>
<meta property="og:image" content="<?php if(is_single()) { $fbthumb = wp_get_attachment_image_src(get_post_thumbnail_id(), 'slider-three'); echo esc_url($fbthumb[0]); } else { echo esc_url(get_option('exm1_facebook_default')); } ?>"/>
<meta property="og:type" content="<?php if(is_single()) { echo "article"; } else { echo "website"; } ?>"/>
<meta property="og:site_name" content="<?php bloginfo('name'); ?>"/>
<meta property="fb:pages" content="378664848920977" />
