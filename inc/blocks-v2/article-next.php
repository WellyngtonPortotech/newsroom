<?php
require_once(get_template_directory() . '/inc/backend/content-proxy.php');
require_once(get_template_directory() . '/inc/blocks/article-teaser.php');

/**
 * Returns the html of "next articles" section.
 * @param string $pageId Page identifier.
 * @param string $sectionId Section (or slot) identifier.
 * @param int $nPosts How many posts to show.
 * @param array $extra Extra parameters expected by ContentProxy.
 * @param int $offset Posts offset.
 * @return string
 * @author Lanza
 */
function getNextArticlesBlock($pageId, $sectionId, $nPosts, $extra, $offset = 0) {
    $posts = ContentProxy::getPosts($pageId, $sectionId, $nPosts, $offset, $extra);
    $wquery = $posts['wpQuery'];
    if($wquery->have_posts()) {
        ob_start();
        ?>
        <section class="article-next-links">
            <header class="article-next-links-header">
                <h2 class="article-next-links-title"><span><?php echo esc_html($posts['title']); ?></span></h2>
            </header>
            <div class="article-next-links-body">
                <?php
                while($wquery->have_posts()) {
                    $wquery->the_post();
                    articleTeaser(true, false);
                }
                wp_reset_postdata();
                ?>
            </div>
        </section>
        <?php
        $ret = ob_get_contents();
        ob_end_clean();
        return $ret;
    } else {
        return '';
    }
}