<?php

/**
 * Get the post category to show in article teaser.
 * @author Lanza
 * @param int|WP_Post $post Post id or post object.
 * @return WP_Category|null
 */
function getCategoryOfPost($post) {
    $excludeSlugs = array('destaque', 'novidades', 'extra', 'home-featured-01', 'home-featured-02', 'home-featured-03', 'home-featured-04');
    $categories = get_the_category($post);
    $nCategories = count($categories);
    if($nCategories) {
        if($nCategories > 1) {
            foreach($categories as $category) {
                if(!in_array($category->slug, $excludeSlugs)) {
                    return $category;
                }
            }
            return $categories[1];
        } else {
            return $categories[0];
        }
    } else {
        return null;
    }
}

/**
 * Prints an article teaser block.
 * @author Lanza
 * @param int|WP_Post $post Post id or post object.
 * @param string $cssClass CSS class to add in the block.
 * @param string $thumbSize Name of thumbnail size (as defined in functions.php).
 * @param string $titleTag Tag to use in title. Default is *h2*.
 */
function articleTeaserBlock($post, $cssClass = '', $thumbSize = 'thumbnail', $titleTag = 'h2') {
    $postDateW3C = date(DATE_W3C, get_the_time('U', $post));
    ?>
    <article class="nv-article-teaser<?php if($cssClass) echo ' ' . $cssClass; ?>" itemscope itemtype="http://schema.org/NewsArticle">
        <div class="nv-article-teaser-content">
            <<?php echo $titleTag; ?> class="nv-article-teaser-title">
                <a href="<?php the_permalink($post); ?>" itemprop="headline"><?php echo esc_html(get_the_title($post)); ?></a>
            </<?php echo $titleTag; ?>>
            <p class="nv-article-teaser-author">
                <a class="nv-article-author" href="<?php the_permalink($post); ?>">
                    <span class="nv-author-name-before">Por</span>
                    <span class="nv-author-name" itemprop="author"><?php the_author($post); ?></span>
                </a>
                <a class="nv-article-date" href="<?php the_permalink($post); ?>" data-date="<?php echo $postDateW3C; ?>">
                    <span class="nv-article-date-before">em</span>
                    <span class="nv-article-date-value" itemprop="datePublished" content="<?php echo $postDateW3C; ?>">
                        <?php echo esc_html(get_the_date("d/m/Y H:i", $post)); ?>
                    </span>
                </a>
            </p>
            <p class="nv-article-teaser-category">
                <span class="no-display">Categoria: </span>
                <?php
                $category = getCategoryOfPost($post);

                if($category) {
                    ?>
                    <a class="nv-article-category" itemprop="genre" href="<?php echo esc_attr(get_term_link($category)); ?>">
                        <?php echo esc_html($category->name); ?>
                    </a>
                    <?php
                } else {
                    ?>
                    <span class="nv-article-category nv-article-no-category" itemprop="genre">Sem categoria</span>
                    <?php
                }
                ?>
            </p>
            <p class="nv-article-teaser-photo">
                <?php
                if(has_post_thumbnail($post)) {
                    $thumbId = get_post_thumbnail_id($post);
                    ?>
                    <a href="<?php the_permalink($post); ?>">
                        <?php
                        the_post_thumbnail($thumbSize, array(
                            'title' => get_the_title($thumbId),
                            'alt' => get_post_meta($thumbId, '_wp_attachment_image_alt', true) || '',
                            'itemprop' => 'thumbnailUrl'
                        ));
                        ?>
                    </a>
                    <?php
                }
                ?>
            </p>
        </div>
    </article>
    <?php
}
