<?php

function getChannelFeedUrl() {
    return 'https://www.youtube.com/feeds/videos.xml?channel_id=UCK81Fr8TPbdsnpE_dImEPVg';
}

function getUrlContent($url) {
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1);
    $output = curl_exec($ch);
    curl_close($ch);
    return $output === false ? '' : $output;
}

function getYoutubeChannelUrl($xml) {
    foreach($xml->link as $link) {
        if(isset($link['rel']) && isset($link['href']) && strtoupper($link['rel']) == 'ALTERNATE') {
            return $link['href'];
        }
    }
    return 'https://www.youtube.com';
}

/**
 * Prints a section with YouTube videos from publisher's channel. Gets the data from YouTube's RSS feed.
 * @param string $title
 * @param int $maxItems
 * @param string $cssClass
 * @param string $titleTag
 * @param string $itemTag
 */
function youtubeTeasers($title = '', $maxItems = 12, $cssClass = '', $titleTag = 'h1', $itemTag = 'h2') {
    try {
        $raw = getUrlContent(getChannelFeedUrl());
        $xml = new SimpleXMLElement($raw);
        ?>
        <div class="nv-youtube-teasers<?php if($cssClass) echo ' ' . $cssClass; ?>">
            <div class="nv-youtube-teasers-content">
                <<?php echo $titleTag; ?> class="nv-youtube-teasers-title">
                    <a href="<?php echo esc_attr(getYoutubeChannelUrl($xml)); ?>"><?php echo esc_html($title); ?></a>
                </<?php echo $titleTag; ?>>
                <div class="nv-youtube-teasers-body nv-slider-area">
                    <div class="nv-youtube-teasers-body-content">
                        <?php
                        $nEntries = max(0, min(count($xml->entry), $maxItems));
                        for($a = 0; $a < $nEntries; $a++) {
                        $entry = $xml->entry[$a];
                        $link = $entry->link->attributes()->href;
                        ?>
                        <div class="nv-youtube-teaser">
                            <<?php echo $itemTag; ?> class="nv-youtube-teaser-title">
                                <a href="<?php echo esc_attr($link); ?>" target="_blank" rel="noopener">
                                    <?php echo esc_html($entry->title); ?>
                                </a>
                            </<?php echo $itemTag; ?>>
                            <p class="nv-youtube-teaser-date">
                                <a href="<?php echo esc_attr($link); ?>" target="_blank" rel="noopener" class="nv-youtube-teaser-date-link" data-date="<?php echo esc_attr($entry->published); ?>">
                                    <span class="nv-youtube-date-before">em</span>
                                    <span class="nv-youtube-date-value">
                                                <?php
                                                $videoDate = date_create_from_format(DATE_W3C, $entry->published);
                                                echo esc_html(date('d/m/Y H:i', $videoDate->getTimestamp()));
                                                ?>
                                            </span>
                                </a>
                            </p>
                            <p class="nv-youtube-teaser-photo">
                                <a href="<?php echo esc_attr($link); ?>" target="_blank" rel="noopener">
                                    <?php
                                    $videoMedia = $entry->children('media', true);
                                    $videoThumb = $videoMedia->group->thumbnail->attributes()->url;
                                    $videoAlt = $videoMedia->group->title;
                                    ?>
                                    <img src="<?php echo esc_attr($videoThumb); ?>" alt="<?php echo esc_attr($videoAlt); ?>"/>
                                </a>
                            </p>
                        </div>
                        <?php
                        }
                        ?>
                </div>
            </div>
        </div>
        <?php
    } catch(Exception $e) {
        echo $e->getMessage();
    }
}
